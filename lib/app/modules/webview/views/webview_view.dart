import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:home_services/app/modules/webview/controllers/webview_controller.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../../../common/ui.dart';
import '../../../providers/laravel_provider.dart';
import '../../../services/settings_service.dart';
import '../../global_widgets/circular_loading_widget.dart';
import '../../global_widgets/block_button_widget.dart';
import '../../../routes/app_routes.dart';

class WebviewView extends GetView<WebviewController> {
  final bool hideAppBar;
  final _key = UniqueKey();
  final _url = "https://jivo.chat/cwsb2RgbI8";

  WebviewView({this.hideAppBar = false});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: hideAppBar
            ? null
            : AppBar(
                title: Text(
                  "Chat chat with us ".tr,
                  style: context.textTheme.headline6,
                ),
                centerTitle: true,
                backgroundColor: Colors.transparent,
                automaticallyImplyLeading: false,
                leading: new IconButton(
                  icon: new Icon(Icons.arrow_back_ios, color: Get.theme.hintColor),
                  onPressed: () => Get.back(),
                ),
                elevation: 0,
              ),
        body: Column(
          children: [
            Expanded(
                child: WebView(
                    key: _key,
                    javascriptMode: JavascriptMode.unrestricted,
                    initialUrl: _url))
          ],
        )
    );
  }
}
